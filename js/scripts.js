 // ABAS DAS ETAPAS
 $('.nav-tabs > li a[title]').tooltip();
 $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
     var $target = $(e.target);
     if ($target.hasClass('disabled')) {
         return false;
     }
 });

//  BOTÃO PARA AVANÇAR - STEPS
 $(".next-step").click(function (e) {
     var $active = $('.etapas .nav-tabs .nav-item .active');
     var $activeli = $active.parent("li");

     $($activeli).next().find('a[data-toggle="tab"]').removeClass("disabled");
     $($activeli).next().find('a[data-toggle="tab"]').click();
 });

//  EXEMPLO: IR PARA UMA ABA OCULTA
 $(".hide-step").click(function (e) {
     $("li#aba_05").find('a[data-toggle="tab"]').click();
 });

//  EXEMPLO: IR PARA UMA ABA OCULTA
 $(".return-step").click(function (e) {
     $("li#aba_03").find('a[data-toggle="tab"]').click();
 });

// CUSTOMIZAÇÃO DOS CHECKBOX E RADIOS
$(document).ready(function(){
  $('input').iCheck({
    checkboxClass: 'icheckbox_minimal',
    radioClass: 'iradio_minimal',
  });
});

// EXEMPLO: DESATIVAR CAMPO DE COMPLEMENTO
$(document).ready(function(){
    $('#formulario_voce input#sem_complemento').on('ifChecked', function () {
        $('#formulario_voce input#complemento').attr('disabled', 'disabled');
    });
    $('#formulario_voce input#sem_complemento').on('ifUnchecked', function () {
        $('#formulario_voce input#complemento').removeAttr('disabled', 'disabled');
    });
});

// EXEMPLO: VALIDAÇÃO DO CHECKBOX (RETIRADA - OUTROS)
$(document).ready(function(){
    $('#formulario_voce #etapa_01 input#realizado_03').on('ifChecked', function () {
        $('#formulario_voce #box_outros').removeClass('d-none');
    });
    $('#formulario_voce #etapa_01 input#realizado_03').on('ifUnchecked', function () {
        $('#formulario_voce #box_outros').addClass('d-none');
    });

    $('#formulario_voce #etapa_02 input#realizado_04').on('ifChecked', function () {
        $('#formulario_voce #box_outros').removeClass('d-none');
    });
    $('#formulario_voce #etapa_02 input#realizado_04').on('ifUnchecked', function () {
        $('#formulario_voce #box_outros').addClass('d-none');
    });
});